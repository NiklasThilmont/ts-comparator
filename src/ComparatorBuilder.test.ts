import * as mocha from 'mocha';
import * as chai from 'chai';
import {ComparatorBuilder, NullMode, nullSafe, TKeyExtractor} from "./ComparatorBuilder";
import {ITestMasterData} from "../dist/ComparatorBuilder.test";

export interface ITestData  {
    id?: number;
    value?: string | null;
    numValue?: number | null;

    getNumValue?(): number;
}

export interface ITestMasterData {
    data?: ITestData;
}


let currentId = 0;
const expect = chai.expect;
describe('Comparator Builder Test(string)', () => {
    it('should be able to build a simple comparator' , () => {
        let a = dummyData("a");
        let b = dummyData("b");
        const comparator = ComparatorBuilder.comparing<ITestData>(t => t.value).build();
        expect(comparator(a, b)).eq("a".localeCompare("b"));
    });

    it('should be able to build an inverse comparator', () => {
        let a = dummyData("a");
        let b = dummyData("b");
        const comparator = ComparatorBuilder.comparing<ITestData>(t => t.value).inverse().build();
        expect(comparator(a, b)).eq("b".localeCompare("a"));
    });

    it('should be able to treat null values as highest order', () => {
        let null_v = dummyData(null);
        let x = dummyData("x");
        let b = dummyData("b");
        let values = [x, b, null_v];
        const comparator = ComparatorBuilder.comparing<ITestData>(t => t.value).definingNullAs(NullMode.HIGHEST).build();
        values = values.sort(comparator);
        expect(values).to.have.ordered.members([null_v, b, x]);
    });

    it('should be able to do stable, null tolerant sorting',  () =>  {
        let null_1 = dummyData(null);
        let null_2 = dummyData(null);
        let z = dummyData("z");
        let b = dummyData("b");
        let x = dummyData("x");
        let y = dummyData("y");
        let values = [null_2, null_1, z, x, y, b];
        const comparator = ComparatorBuilder.comparing<ITestData>(t => t.value).definingNullAs(NullMode.HIGHEST).build();
        values = values.sort(comparator);
        expect(values).to.have.ordered.members([null_2, null_1, b, x, y, z]);
    });

    it('should be able to do stable, null tolerant sorting where null is lowest', () => {
        let null_1 = dummyData(null);
        let null_2 = dummyData(null);
        let z = dummyData("z");
        let b = dummyData("b");
        let x = dummyData("x");
        let y = dummyData("y");
        let values = [null_2, null_1, z, x, y, b];
        const comparator = ComparatorBuilder.comparing<ITestData>(t => t.value).definingNullAs(NullMode.LOWEST).build();
        values = values.sort(comparator);
        expect(values).to.have.ordered.members([b, x, y, z, null_2, null_1]);
    });

    it('should be able to do a stable sort', () => {
        let z = dummyData("z");
        let b = dummyData("b");
        let x = dummyData("x");
        let y = dummyData("y");
        let values = [z, x, y, b];
        const comparator = ComparatorBuilder.comparing<ITestData>(t => t.value).build();
        values = values.sort(comparator);
        expect(values).to.have.ordered.members([b, x, y, z]);
    });

    it('should be able to handle equal values', function () {
        let z1 = dummyData("z");
        let z2 = dummyData("z");
        let b = dummyData("b");
        let values = [z1, b, z2];
        const comparator = ComparatorBuilder.comparing<ITestData>(t => t.value).build();
        values = values.sort(comparator);
        expect(values).to.have.ordered.members([b, z1, z2]);
    });
});

describe("Comparator builder test(number)", () => {
    it('should be able to build a simple comparator', () => {
        let one = numberDummyData(1);
        let six = numberDummyData(6);
        let five = numberDummyData(5);
        let values = [one, six, five];
        const comparator = ComparatorBuilder.comparing<ITestData>(t => t.numValue).build();
        values = values.sort(comparator);
        expect(values).to.have.ordered.members([one, five, six]);
        const inverse = ComparatorBuilder.comparing<ITestData>(t => t.numValue).inverse().build();
        values = values.sort(inverse);
        expect(values).to.have.ordered.members([six, five, one]);
    })
});

describe("Multi Level comparison", () => {
    it('should be able to build a simple comparator', () => {
        let a5 = dummyData("a", 5);
        let a2 = dummyData("a", 2);
        let a7 = dummyData("a", 7);
        let g1 = dummyData("g", 1);
        let g6 = dummyData("g", 6);
        let f1 = dummyData("f", 1);
        let values = [a5, a2, a7, g6, g1, f1];
        let expected = [a2, a5, a7, f1, g1, g6];
        const comparator = ComparatorBuilder.comparing<ITestData>(t => t.value).thenComparing(t => t.numValue).build();
        let sorted = values.sort(comparator);
        expect(sorted).to.have.ordered.members(expected);
        const comparatorInverse = ComparatorBuilder.comparing<ITestData>(t => t.value).thenComparing(t => t.numValue).inverse().build();
        sorted = values.sort(comparatorInverse);
        expect(sorted).to.have.ordered.members(expected.reverse());
    });
});

describe("Null safe test", () => {
    it('should evaluate to null if any value is null', function () {
        const extractor: TKeyExtractor<ITestMasterData> = nullSafe(t => t.data.numValue);
        const value = extractor(null);
        expect(value).eq(null);
    });

    it('should work in conjunction with a comparator', function () {
        const comparator = ComparatorBuilder
            .comparing<ITestMasterData>(nullSafe(t => t.data.value))
            .definingNullAs(NullMode.HIGHEST)
            .build();
        const valueA:ITestMasterData = {
            data: dummyData("A")
        };
        const valueNull: ITestMasterData = {
            data: null,
        };
        let sorted = [valueA, valueNull].sort(comparator);
        expect(sorted).to.have.ordered.members([valueNull, valueA]);
    })
});


export const dummyData = (val: string | null, numValue?: number | null): ITestData => {
    return {
        id: currentId++,
        value: val,
        numValue: numValue
    }
};

export const numberDummyData = (val: number) => {
    return  {
        id: currentId++,
        numValue: val
    }
};