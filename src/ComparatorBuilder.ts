export type TBasicComparable = string | number | boolean | undefined | null;

export type TKeyExtractor<T>= (t: T) => TBasicComparable;

export type CompareFunction<T> = (v1: T, v2: T) => number;

export function nullSafe<T>(keyExtractor: TKeyExtractor<T>): TKeyExtractor<T> {
    return (value: T) => {
        try {
            return keyExtractor(value);
        } catch (error) {
            if (error instanceof TypeError) {
                return null;
            } else {
                throw error;
            }
        }
    }
}

/**
 * Defines how <code>null</code> or <code>undefined</code> are treated when encountering. </br>
 * A <code>NullMode</code> of LOWEST defines null-values as lowest. The array ["a", "d", null, "c"] would be sorted
 * as ["a", "c", "d", null].
 * A <code>NullMode</code> of <code>HIGHEST</code> sorts priorizes null values. The array ["a", "d", null, "c"] would be sorted
 * as [null, "a", "c", "d"].
 */
export enum NullMode {
    /**
     * Priorizes non-null values. The array ["a", "d", null, "c"] would be sorted
     * as ["a", "c", "d", null].
     */
    LOWEST,

    /**
     * Priorizes null values. The array ["a", "d", null, "c"] would be sorted
     * as [null, "a", "c", "d"].
     */
    HIGHEST,
    /**
     * No null treatment.
     */
    NONE
}

/**
 * Sample usages:
 * <ul>
 *     <li><code>ComparatorBuilder<Person>.comparing(person => person.name).build()</code></li>
 *     <li><code>ComparatorBuilder<Person>.comparing(person => person.name).thenComparing(person => person.age).build()</code></li>
 *     <li><code>ComparatorBuilder<Person>.comparing(person => person.name).definingNullAs(LOWEST).build()</code></li>
 * </ul>
 */
export class ComparatorBuilder<T> {
    private extractors: Array<TKeyExtractor<T>> = [];
    private inverseFlag: boolean = false;
    private nullMode: NullMode = NullMode.NONE;


    private constructor(initialExtractor: TKeyExtractor<T>) {
        this.extractors.push(initialExtractor);
    }

    /**
     * Creates a builder that compares <code>T</code> by the values extracted by the provided <code>keyExtractor</code>.
     * @param {TKeyExtractor<T>} keyExtractor
     * @returns {ComparatorBuilder<T>}
     */
    public static comparing<T>(keyExtractor: TKeyExtractor<T>): ComparatorBuilder<T> {
        return new ComparatorBuilder<T>(keyExtractor);
    }

    /**
     * Reverses the whole comparison result. Reversion is done on the FINAL comparison result, not on the individual ones.
     */
    public inverse = (): ComparatorBuilder<T> => {
        this.inverseFlag = true;
        return this;
    };

    /**
     * See {@link NullMode}. </br>
     * The default NullMode is {@link NullMode.NONE}. With NONE, null values will cause an exception.
     * @param {NullMode} nullMode to be used
     * @returns {ComparatorBuilder<T>} this builder.
     */
    public definingNullAs = (nullMode: NullMode): ComparatorBuilder<T> => {
        this.nullMode = nullMode;
        return this;
    };

    public thenComparing = (nextExtractor: TKeyExtractor<T>): ComparatorBuilder<T> => {
        this.extractors.push(nextExtractor);
        return this;
    };

    public build(): CompareFunction<T> {
        if (this.extractors.length === 0) {
            throw new Error("No Extractor Provided!");
        }
        let factor = 1;
        if (this.inverseFlag) {
            factor = -1;
        }
        return (t1, t2) => (this.compare(t1, t2, [...this.extractors], 0)) * factor;
    }

    private compare = (t1: T, t2: T, extractors: Array<TKeyExtractor<T>>, depth: number) => {
        const v1 = extractors[depth](t1);
        const v2 = extractors[depth](t2);
        let res = 0;
        /** Null Value Control */
        if ((v1 === null || v2 === null) && this.nullMode === NullMode.NONE) {
            throw new Error("NullMode is NONE but one of the values is null!");
        }
        if (v1 === null && v2 === null && this.nullMode !== NullMode.NONE) {
            if ((depth + 1)< extractors.length) {
                res = this.compare(t1, t2, extractors, depth + 1);
            }
            else {
                res = 0;
            }
        }
        else if (!v1 && this.nullMode === NullMode.HIGHEST && !!v2) {
            res = -1;
        }
        else if (!v2 && this.nullMode === NullMode.HIGHEST && !!v1)  {
           res = 1;
        }
        else if (!v1 && this.nullMode === NullMode.LOWEST && !!v2) {
            res = 1;
        }
        else if (!v2 && this.nullMode === NullMode.LOWEST && !!v1)  {
            res = -1;
        }
        else if (v1 > v2) {
            res = 1;
        }
        else if (v2 > v1) {
            res = -1;
        }
        else if (v1 === v2 && ((depth + 1) < extractors.length)) {
            res = this.compare(t1, t2, extractors, depth + 1);
        }
        return res;
    }
}