import * as mocha from 'mocha';
import * as chai from 'chai';
import {ComparatorBuilder, NullMode} from "./ComparatorBuilder";
import {dummyData, ITestData} from "./ComparatorBuilder.test";

function randString(lenght: number) {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < lenght; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}

const compare = (d1: ITestData, d2: ITestData) => {
    if (d1.value > d2.value) {
        return 1;
    }
    if (d2.value > d1.value) {
        return -1;
    }
    return 0;
};

interface BenchResult {
    qty: number;
    iterations?: number;
    timeNormal: number;
    timeBuilder: number;
}

const expect = chai.expect;
describe('Comparator Builder Performance Test', () => {
    const runTest = (testQuantity: number) => {
        const testDataNormal: Array<ITestData> = [];
        const testDataForComparison: Array<ITestData> = []
        // Step 1 - create lots of test data (twice)
        for(let i = 0; i < testQuantity; i++) {
            let val = dummyData(randString(10));
            testDataNormal.push(val);
            testDataForComparison.push(val);
        }
        const tNormal0 = new Date().getTime();
        testDataNormal.sort(compare);
        const tNormal2 = new Date().getTime();

        const tBuilder0 = new Date().getTime();
        const comparator = ComparatorBuilder.comparing<ITestData>(t => t.value).definingNullAs(NullMode.HIGHEST).build();
        testDataForComparison.sort(comparator);
        const tBuilder1 = new Date().getTime();
        return {
            qty: testQuantity,
            timeNormal: tNormal2 - tNormal0,
            timeBuilder: tBuilder1 - tBuilder0
        }
    };

    const runTestIterated = (qty: number, iterations: number) => {
        let current = {qty: qty, timeBuilder: 0, timeNormal: 0};
      for (let i = 0; i < iterations; i++) {
          let res = runTest(qty);
        current = mergeResults(current, res);
      }
      current.timeBuilder = current.timeBuilder / iterations;
      current.timeNormal = current.timeNormal / iterations;
      console.log("Results for " + iterations + " iterations and " + qty + " values:");
      console.log("Avg. normal: " + current.timeNormal + " ms.");
      console.log("Avg. builder: " + current.timeBuilder + " ms.");
      console.log("");
    };

    const mergeResults = (r1: BenchResult, r2: BenchResult): BenchResult =>  {
        return {
            qty: r1.qty,
            timeBuilder: r1.timeBuilder + r2.timeBuilder,
            timeNormal: r1.timeNormal + r2.timeNormal
        }
    };

    it('Should so some comparisons' , () => {
        runTestIterated(500, 100);
        runTestIterated(5000, 100);
        runTestIterated(50000, 100);
    });
});